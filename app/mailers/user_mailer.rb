class UserMailer < ActionMailer::Base
  default from: "postmaster@app21316728.mailgun.org"

  def welcome_email(user)
    @user = user
    @url  = 'http://www.test.com/login'
    mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  end

  def league_invitation_email(sender,league,receiver_email)
    @sender = sender
    @league = league
    @url = 'http://www.test.com/validate'
    mail(from: %q{invites@jogamas.com},to:receiver_email, subject: %q{There's a new invitation for you! @ Jogamas})
  end

  def password_reset_email(user, url)
    @user = user
    @url = url
    mail(from: %q{passwordreset@jogamas.com}, to:@user.email, subject: 'Password reset request')

  end

  def user_league_invitation_email(sender,league,receiver_user)
    @sender = sender
    @league = league
    @url = 'http://www.test.com/validate'
    mail(from: %q{invites@jogamas.com},to:receiver_user.email, subject: %q{There's a new invitation for you! @ Jogamas})
  end

  def user_league_resend_email(sender,league,receiver_user)
    @sender = sender
    @league = league
    @url = 'http://www.test.com/validate'
    mail(from: %q{invites@jogamas.com},to:receiver_user.email, subject: %q{your friend is waiting for you! @ Jogamas})
  end

end
