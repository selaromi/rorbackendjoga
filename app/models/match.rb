class Match < ActiveRecord::Base
  belongs_to :group
  belongs_to :local_team, :class_name => "Team", :foreign_key => :local_team
  belongs_to :visitor_team, :class_name => "Team", :foreign_key => :visitor_team
  belongs_to :stadium

  def Match.updateSecondRoundMatches
    #  Get All Teams
    # Ordered Teams
    orderedTeams = Team.all.order(:group_id,points: :desc,goals_scored: :desc,goals_received: :asc)

    # if the sum of games_won, games_lost, games_tied > 0, the team has at least played a single game
    # (so while they haven't played a game, they will not be included in the forecast
    # onlye the first and second positions of each league will be taken into account
    # for 0 - 7 (A to H),
    # 4 teams per group: 4 * group and (4 * group) + 1 represent the first and second of each group
    # Round of 16 has fixed id so we'll user them

    Match.find(49).update_attribute(:local_team, orderedTeams[0]) if (orderedTeams[0].games_won + orderedTeams[0].games_lost + orderedTeams[0].games_tied > 0)
    Match.find(49).update_attribute(:visitor_team, orderedTeams[5]) if (orderedTeams[5].games_won + orderedTeams[5].games_lost + orderedTeams[5].games_tied > 0)
    Match.find(50).update_attribute(:local_team, orderedTeams[8]) if (orderedTeams[8].games_won + orderedTeams[8].games_lost + orderedTeams[8].games_tied > 0)
    Match.find(50).update_attribute(:visitor_team, orderedTeams[13]) if (orderedTeams[13].games_won + orderedTeams[13].games_lost + orderedTeams[13].games_tied > 0)
    Match.find(51).update_attribute(:local_team, orderedTeams[4]) if (orderedTeams[4].games_won + orderedTeams[4].games_lost + orderedTeams[4].games_tied > 0)
    Match.find(51).update_attribute(:visitor_team, orderedTeams[1]) if (orderedTeams[1].games_won + orderedTeams[1].games_lost + orderedTeams[1].games_tied > 0)
    Match.find(52).update_attribute(:local_team, orderedTeams[12]) if (orderedTeams[12].games_won + orderedTeams[12].games_lost + orderedTeams[12].games_tied > 0)
    Match.find(52).update_attribute(:visitor_team, orderedTeams[9]) if (orderedTeams[9].games_won + orderedTeams[9].games_lost + orderedTeams[9].games_tied > 0)
    Match.find(53).update_attribute(:local_team, orderedTeams[16]) if (orderedTeams[16].games_won + orderedTeams[16].games_lost + orderedTeams[16].games_tied > 0)
    Match.find(53).update_attribute(:visitor_team, orderedTeams[21]) if (orderedTeams[21].games_won + orderedTeams[21].games_lost + orderedTeams[21].games_tied > 0)
    Match.find(54).update_attribute(:local_team, orderedTeams[24]) if (orderedTeams[24].games_won + orderedTeams[24].games_lost + orderedTeams[24].games_tied > 0)
    Match.find(54).update_attribute(:visitor_team, orderedTeams[29]) if (orderedTeams[29].games_won + orderedTeams[29].games_lost + orderedTeams[29].games_tied > 0)
    Match.find(55).update_attribute(:local_team, orderedTeams[20]) if (orderedTeams[20].games_won + orderedTeams[20].games_lost + orderedTeams[20].games_tied > 0)
    Match.find(55).update_attribute(:visitor_team, orderedTeams[17]) if (orderedTeams[17].games_won + orderedTeams[17].games_lost + orderedTeams[17].games_tied > 0)
    Match.find(56).update_attribute(:local_team, orderedTeams[28]) if (orderedTeams[28].games_won + orderedTeams[28].games_lost + orderedTeams[28].games_tied > 0)
    Match.find(56).update_attribute(:visitor_team, orderedTeams[25]) if (orderedTeams[25].games_won + orderedTeams[25].games_lost + orderedTeams[25].games_tied > 0)
  end

  def Match.create_round_of_8_matches match
    case match.id
    when 49
      Match.find(57).update_attribute(:local_team, match.winner_team)
    when 50
      Match.find(57).update_attribute(:visitor_team, match.winner_team)
    when 51
      Match.find(59).update_attribute(:local_team, match.winner_team)
    when 52
      Match.find(59).update_attribute(:visitor_team, match.winner_team)
    when 53
      Match.find(58).update_attribute(:local_team, match.winner_team)
    when 54
      Match.find(58).update_attribute(:visitor_team, match.winner_team)
    when 55
      Match.find(60).update_attribute(:local_team, match.winner_team)
    when 56
      Match.find(60).update_attribute(:visitor_team, match.winner_team)
    end
  end

  def Match.create_semifinals_matches match
    case match.id
      when 57
        Match.find(61).update_attribute(:local_team, match.winner_team)
      when 58
        Match.find(61).update_attribute(:visitor_team, match.winner_team)
      when 59
        Match.find(62).update_attribute(:local_team, match.winner_team)
      when 60
        Match.find(62).update_attribute(:visitor_team, match.winner_team)
    end
  end

  def Match.create_finals_matches match
    case match.id
      when 61
        Match.find(64).update_attribute(:local_team, match.winner_team)
        Match.find(63).update_attribute(:local_team, match.winner_team != match.local_team ? match.local_team : match.visitor_team)
      when 62
        Match.find(64).update_attribute(:visitor_team, match.winner_team)
        Match.find(63).update_attribute(:visitor_team, match.winner_team != match.local_team ? match.local_team : match.visitor_team)
    end
  end

end
