class User < ActiveRecord::Base
    include SmsCommunication
    include JogaFbGraphManager
    include JogaUtils

    devise  :database_authenticatable, :registerable,
            :recoverable, :rememberable, :trackable, :validatable, :confirmable,
            :omniauthable, :omniauth_providers => [:facebook]
    before_save :ensure_authentication_token
    after_save :add_initial_data

    has_one :user_settings
    has_many :timeline_events
    has_many :predictions
    has_many :user_leagues
    has_many :leagues, through: :user_leagues # this wasn't working, solution found: http://stackoverflow.com/questions/1781202/could-not-find-the-association-problem-in-rails

    has_many :user_badges
    has_many :badges, through: :user_badges

  	validates :firstName, :lastName, :username, presence:true
    validates :username, uniqueness: true
    validates :email, format: { with: VALID_EMAIL_REGEX }
    acts_as_voter

    def self.find_for_facebook_oauth(auth)
      return add_facebook_to_user(auth) if auth.info.email && where("email = ?", auth.info.email).any?

      where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
        
        user.set_user_from_facebook! auth
        user.exchange_token auth.credentials.token

        # fb_auth = FbGraph::Auth.new(ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_SECRET'])
        # fb_auth.exchange_token! auth.credentials.token # => Rack::OAuth2::AccessToken
        # user.oauth_token = fb_auth.access_token.to_s
        # user.oauth_expires_at = user.oauth_expires_at = Time.at(auth.credentials.expires_at) unless auth.credentials.expires_at.nil?
        
        user.skip_confirmation!
        if(user.save!)

          userSettings = UserSettings.new
          location = auth.info.location.split(',') if (!auth.info.location.nil?)

          if(location.any?)
            userSettings.country = location[1].strip unless location[1].strip.empty?
            userSettings.city = location[0].strip unless location[0].strip.empty? 
          end

          userSettings.age = (auth.info.age.presence || '')
          userSettings.sex = (auth.extra.raw_info.gender == 'male') ? '0' : '1'
          userSettings.interests = ''
   
          if auth.extra.raw_info.favorite_teams.presence
            auth.extra.raw_info.favorite_teams.each { |e|  
              userSettings.interests += "#{e.name},"
            } 
          end

          userSettings.user_id = user.id
          if userSettings.save
            user.timeline_events << TimelineEvent.create(event_type:"Joined",data:nil)
            user.update_badge 'REGISTRATION', 1
            user.welcome_msg # Envia mensaje de FB de bienvenida si el usuario tiene número de móvil.
            UserMailer.welcome_email(user).deliver
          else
            User.destroy(user.id)
            return nil
          end
        end
        return user
      end
      return nil
    end



    def set_user_from_facebook! auth
      self.provider = auth.provider
      self.uid = auth.uid
      self.firstName = auth.info.first_name
      self.lastName = auth.info.last_name
      self.username = self.get_valid_username [auth.info.nickname, auth.info.first_name]
      self.password = Devise.friendly_token[0,20]
      self.email = auth.info.email
    end

    def get_valid_username names
      @username = ""
      i = 0
      names.each do |name|
        return name if name && User.where("username = ?",name).empty?
      end

      begin
        i = i + 1
        @username = "#{names[1]}_#{i.to_s}"
      end until where("username = '#{@username}'").empty?
      return @username
    end

    def self.add_facebook_to_user auth 
      where(email:auth.info.email).first.tap do |user|
        user.provider ||= auth.provider
        user.uid ||= auth.uid
        user.exchange_token auth.credentials.token
        user.save!
        #user.welcome_msg
        return user
      end
      return nil
    end
    
    def self.find_for_database_authentication(conditions={})
      self.where("username = ?", conditions[:username]).limit(1).first ||
      self.where("email = ?", conditions[:username]).limit(1).first
    end

    def update_badge badge_type, new_progress_value
      badge = Badge.find_by(badge_type: badge_type)
      user_badge = UserBadge.find_by user_id: self.id, badge_id: badge.id
      if new_progress_value < badge.goal
        return user_badge.update(progress: new_progress_value)
      elsif new_progress_value == badge.goal
        user_badge.update(progress: new_progress_value, won: true)
        add_new_event TimelineEvent.create(event_type:'newBadge',data:badge.to_json( :only => [:name,:id]))
      end
    end

    def add_new_badge badge
      self.badges << badge
    end

    def add_new_event event
      self.timeline_events << event
    end

    private

    def add_initial_data
      Badge.all.each do |badge|
          self.badges << badge unless self.badges.include? badge
      end
      self.user_leagues << UserLeague.new({league_id: 0, user_id: self.id, status:1}) unless UserLeague.where('user_leagues.league_id=? AND user_leagues.user_id=?',0,self.id).any?
    end

    def ensure_authentication_token
    	if authentication_token.blank?
    		self.authentication_token = generate_authentication_token
    	end
    end

    private
    def generate_authentication_token
    	loop do
    		token = Devise.friendly_token
    		break token unless User.where(authentication_token: token).first
    	end
    end

    protected
    def confirmation_required?
      false
    end
end


# def self.find_for_facebook_oauth(auth)
#   where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
#     user.provider = auth.provider
#     user.uid = auth.uid
#     user.email = auth.info.email
#     user.password = Devise.friendly_token[0,20]
#     user.name = auth.info.name   # assuming the user model has a name
#     user.image = auth.info.image # assuming the user model has an image
#     user.save!
#   end
# end