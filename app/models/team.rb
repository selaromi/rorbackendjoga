class Team < ActiveRecord::Base
  belongs_to :group

  def Team.update_teams_points match
    if(match.winner_team)
      Team.find(match.winner_team).increment!(:points, 3)
    else
      Team.find(match.local_team).increment!(:points)
      Team.find(match.visitor_team).increment!(:points)
    end
  end
end
