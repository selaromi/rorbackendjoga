class League < ActiveRecord::Base
  validates :name, :description, :user_id, presence: true
  belongs_to :user
  has_many :user_league
  has_many :users, through: :user_league
  has_many :comments

  def add_comment comment
    self.comments << comment
  end
end
