class EmailLeague < ActiveRecord::Base
  def EmailLeague.update_pending_invitations user
    pendingInvitations = EmailLeague.where('email = ? OR facebook_id = ?',user.email,user.uid)
    pendingInvitations.each do |invitation|
      ul = UserLeague.new(user_id: user.id, league_id: invitation.league_id, status:0)
      ul.save()
    end
  end
end
