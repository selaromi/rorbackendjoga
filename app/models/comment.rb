class Comment < ActiveRecord::Base
  belongs_to :sender_id, :class_name => "User", :foreign_key => :sender_id
  validates :league_id, :comment, :sender_id, presence:true
  acts_as_votable

  def get_latest_comments
    self.comments.last(10).reverse.to_json
  end
end
