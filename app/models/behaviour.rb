class Behaviour
  include MongoMapper::Document

  key :type,        String
  key :content,      String
  key :published_at, Time
  timestamps!
end
