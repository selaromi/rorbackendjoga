class Prediction < ActiveRecord::Base
  belongs_to :user
  belongs_to :match

  def update_points newPoints
    self.update_attribute(:points, newPoints)
  end
end
