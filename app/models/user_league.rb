class UserLeague < ActiveRecord::Base
	validates :status, :user_id, :league_id, presence:true
	belongs_to :user
	belongs_to :league
end
