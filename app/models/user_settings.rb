class UserSettings < ActiveRecord::Base
	belongs_to :user

  def update_points newPoints
    increment(:points, newPoints)
    save
  end


  def update_successful_prediction prediction
    teamRegions = [prediction.match.local_team[:region],prediction.match.visitor_team[:region]]
    teamRegions.each do |region|
      increment(:predictions_successful_america) if region == "Am"
      increment(:predictions_successful_africa) if region == "Af"
      increment(:predictions_successful_europe) if region == "Eu"
      increment(:predictions_successful_oceania) if region == "Oc"
      increment(:predictions_successful_asia) if region == "As"
    end
    save
  end

  def update_perfect_prediction
    increment(:predictions_perfect)
    save
  end

end
