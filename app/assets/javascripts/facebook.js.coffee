    jQuery ->
      $('body').prepend('<div id="fb-root"></div>')
    
      $.ajax
        url: "#{window.location.protocol}//connect.facebook.net/en_US/all.js"
        dataType: 'script'
        cache: true
    
    window.fbAsyncInit = ->
      FB.init(appId: '212391085617615', cookie: true)
    
      $('#sign_in').click (e) ->
        e.preventDefault()
        FB.login (response) ->
          #console.log(response)
          window.location = '/users/auth/facebook/callback?signed_request='+response.authResponse.signedRequest if response.authResponse
        ,{ scope: 'email, user_interests, publish_actions'}    
      $('#sign_out').click (e) ->
        FB.getLoginStatus (response) ->
          FB.logout() if response.authResponse
        true 