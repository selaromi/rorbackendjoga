class MatchObserver < ActiveRecord::Observer
  observe :match

  def after_save match
    Match.updateSecondRoundMatches if(match.id < 49)
    Team.update_teams_points match
    begin
      base = ENV['BASE_POINTS'].to_i
      bonus = ENV['BONUS_POINTS'].to_i
      winner_predictions = Array.new()
      Prediction.where('match_id = ? AND winner_team = ?', match.id, match.winner_team).each { |prediction|
        newPoints = base
        newPoints += bonus if(prediction.local_goals == match.local_goals && prediction.visitor_goals == match.visitor_goals)
        prediction.update_points newPoints
        winner_predictions << prediction
      }

      winner_predictions.each { |prediction|
        user = User.find(prediction.user_id)
        add_update_points_event user, prediction
        update_user_settings user, prediction

        if (prediction.local_goals == prediction.visitor_goals &&
            prediction.local_goals == match.local_goals &&
            prediction.visitor_goals == match.visitor_goals &&
            match.id >= 49)
          user.update_badge 'PENALTIES', 1
          #add_badge_and_event user, "PENALTIES" unless UserBadge.where(user_id: user.id, badge_id: Badge.find_by(badge_type: "PENALTIES").id).any?
        end

        add_badge_and_event user, "CHAMP" if(match.id == 64)
      }

      #at this point, we know which team won the final
      if(match.id == 64)
        UserSettings.where("favorite_team = ?", match.winner_team).each { |user_settings|
          user.update_badge 'FAV', 1
        }
      end

      Match.create_round_of_8_matches match if(match.id >= 49 && match.id <= 56)
      Match.create_semifinals_matches match if(match.id >= 57 && match.id <= 60)
      Match.create_finals_matches match if(match.id >= 61 && match.id <= 62)

      set_quarter_finals_badges if (match.id == 56)
      set_semi_finals_badges if (match.id == 60)
      set_final_badges if (match.id == 62)

    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
    end
  end

  def set_phase_badges previous_matches_teams, predicted_matches_teams, success_badge_type
    teams = Match.find(previous_matches_teams).collect(&:local_team)
    teams << Match.find(previous_matches_teams).collect(&:visitor_team)
    teams.flatten!.sort!

    #get teams that are there, and get users whose predictions coincide with them
    User.all.map { |user|
      predicted_teams = Prediction.where("match_id in (#{predicted_matches_teams.join(',')}) AND user_id = ?", user.id).collect(&:winner_team).sort
      if (teams == predicted_teams)
        user.update_badge success_badge_type, 1
        #add_badge_and_event user, "FINAL"
      end
    }
  end
  def set_final_badges
    set_phase_badges [64],[61,62], 'FINAL'
    #teams = Match.find([64]).collect(&:local_team)
    #teams << Match.find([64]).collect(&:visitor_team)
    #teams.flatten!.sort!
    #
    ##get teams that are there, and get users whose predictions coincide with them
    #User.all.map { |user|
    #  predicted_teams = Prediction.where("match_id in (61,62) AND user_id = ?", user.id).collect(&:winner_team).sort
    #  if (teams == predicted_teams)
    #    user.update_badge 'FINAL', 1
    #    #add_badge_and_event user, "FINAL"
    #  end
    #}
  end

  def set_semi_finals_badges
    set_phase_badges [61,62],[57,58,59,60], 'SEMI'
    #teams = Match.find([61, 62]).collect(&:local_team)
    #teams << Match.find([61, 62]).collect(&:visitor_team)
    #teams.flatten!.sort!
    #
    ##get teams that are there, and get users whose predictions coincide with them
    #User.all.map { |user|
    #  predicted_teams = Prediction.where("match_id in (57,58,59,60) AND user_id = ?", user.id).collect(&:winner_team).sort
    #  if (teams == predicted_teams)
    #    user.update_badge 'SEMI', 1
    #    #add_badge_and_event user, "SEMI"
    #  end
    #}
  end

  def set_quarter_finals_badges
    set_phase_badges [57, 58, 59, 60],[49,50,51,52,53,54,55,56], 'QUARTER'
    #teams = Match.find([57, 58, 59, 60]).collect(&:local_team)
    #teams << Match.find([57, 58, 59, 60]).collect(&:visitor_team)
    #teams.flatten!.sort!
    #
    ##get teams that are there, and get users whose predictions coincide with them
    #User.all.map { |user|
    #  predicted_teams = Prediction.where('match_id in (49,50,51,52,53,54,55,56) AND user_id = ?', user.id).collect(&:winner_team).sort
    #  if (teams == predicted_teams)
    #    user.update_badge 'QUARTER', 1
    #    #add_badge_and_event user, "QUARTER"
    #  end
    #}
  end


  def update_user_settings user, prediction
    user.user_settings.update_points prediction.points
    user.user_settings.update_successful_prediction prediction
    user.reload

    africa  = user.user_settings.predictions_successful_africa
    america = user.user_settings.predictions_successful_america
    europe  = user.user_settings.predictions_successful_europe
    asia    = user.user_settings.predictions_successful_asia
    oceania = user.user_settings.predictions_successful_oceania
    total_correct_predictions =  (africa + america + europe + asia + oceania) / 2

    if prediction.points == (ENV['BASE_POINTS'].to_i + ENV['BONUS_POINTS'].to_i)
      user.user_settings.update_perfect_prediction
      user.reload
    end

    #update all badges
    user.update_badge '1PREDICTION', total_correct_predictions
    user.update_badge '5PREDICTION', total_correct_predictions
    user.update_badge '5AMERICA', america
    user.update_badge '5AFRICA', africa
    user.update_badge '5ASIA', asia + oceania
    #user.update_badge '5OCEANIA', oceania
    user.update_badge '5EUROPE', europe
    user.update_badge '1SCORE', user.user_settings.predictions_perfect
    user.update_badge '5SCORE', user.user_settings.predictions_perfect
  end

  def add_update_points_event (user, prediction)
    user.add_new_event TimelineEvent.create(event_type:'updatePoints',data:prediction.to_json( :only => [:points,:match_id]))

  end

  def add_badge_and_event (user, new_badge_type)
    user.add_new_badge Badge.find_by(badge_type: new_badge_type)
    user.add_new_event TimelineEvent.create(event_type:'newBadge',data:Badge.find_by(badge_type: new_badge_type).to_json( :only => [:name,:id]))
  end
end