class StadiaController < ApplicationController
  before_action :set_stadium, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate_user_from_token!, :only => [ :all ]
  skip_before_filter :authenticate_user!, :only => [:all]

  # GET /stadia
  def index
    @stadia = Stadium.all
  end

  # post /stadia
  def all
    render json: Stadium.all.to_json, status: :ok
  end
  # GET /stadia/1
  def show
  end

  # GET /stadia/new
  def new
    @stadium = Stadium.new
  end

  # GET /stadia/1/edit
  def edit
  end

  # POST /stadia
  def create
    @stadium = Stadium.new(stadium_params)

    if @stadium.save
      redirect_to @stadium, notice: 'Stadium was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /stadia/1
  def update
    if @stadium.update(stadium_params)
      redirect_to @stadium, notice: 'Stadium was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /stadia/1
  def destroy
    @stadium.destroy
    redirect_to stadia_url, notice: 'Stadium was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stadium
      @stadium = Stadium.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def stadium_params
      params[:stadium]
    end
end
