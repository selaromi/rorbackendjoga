require 'pp'

class SessionsController < Devise::SessionsController
  include Devise::Controllers::Helpers
  prepend_before_filter :require_no_authentication, :only => [:create, :createFromFacebook ]
  skip_before_filter :authenticate_user_from_token!, :only => [ :create, :createFromFacebook ]
  skip_before_filter :authenticate_user!, :only => [:create, :createFromFacebook ]
  respond_to :json
  
  def create
    resource = User.find_for_database_authentication({username: params['user']['username']})
    return render json: {error:"usuario o password errado"}, status: :unprocessable_entity unless resource.presence && resource.valid_password?(params[:user][:password])

    #self.resource = warden.authenticate!(auth_options)

    sign_in(resource_name, resource)
    resource.authentication_token = reset_authentication_token(resource)
    return render json: resource.errors, status: :unprocessable_entity unless resource.save!
    puts current_user.id
    render json: {
      auth_token: resource.authentication_token,
      username: resource.username,
      fb_token: resource.uid,
    }, status: :ok
  end

  def destroy
    sign_out(resource_name)
    render json: {
      status: "200"
    }
  end

  def createFromFacebook
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    redirect_to root_url
  end

  def destroyFromFacebook
    session[:user_id] = nil
    redirect_to root_url
  end
end
