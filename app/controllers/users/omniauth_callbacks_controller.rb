class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  # before_filter :cors_preflight_check
  # after_filter :cors_set_access_control_headers
  skip_before_filter :authenticate_user_from_token!

  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.find_for_facebook_oauth(request.env["omniauth.auth"])
    if @user.persisted?

      sign_in @user, store: false #this will throw if @user is not activated
      @user.authentication_token = reset_authentication_token(@user)
      @user.save!
      EmailLeague.update_pending_invitations @user
      render json: {
        auth_token: @user.authentication_token,
        username: @user.username,
        fb_token: @user.uid,
      }, status: :ok
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      render json: @user.errors
    end
  end

  # def cors_preflight_check
  #   if request.method == :options
  #     logger.warning :options
  #     headers['Access-Control-Allow-Origin'] = '*'
  #     headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
  #     headers['Access-Control-Request-Method'] = '*'
  #     headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  #     render :text => '', :content_type => 'text/plain'
  #   end
  # end

  # def cors_set_access_control_headers
  #   headers['Access-Control-Allow-Origin'] = '*'
  #   headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
  #   headers['Access-Control-Allow-Headers'] = '*'
  #   headers['Access-Control-Max-Age'] = "1728000"
  # end
end