class MatchesController < ApplicationController
  skip_before_filter :authenticate_user_from_token!, :only => [ :all ]
  skip_before_filter :authenticate_user!, :only => [:all]

  def all
    render json: Match.all.to_json, status: :ok
  end

end
