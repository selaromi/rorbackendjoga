class RegistrationsController  < Devise::RegistrationsController
  skip_before_filter :verify_authenticity_token, :only => [:create ]
  skip_before_filter :authenticate_user_from_token!, :only => [:create ]
  respond_to :json

  def create
    @user = User.new(user_params)
    render json: @user.errors, status: :unprocessable_entity unless @user.save!
    @user.add_new_event TimelineEvent.create(event_type:"Joined",data:nil)
    @user.update_badge "REGISTRATION", 1
    SmsCommunication::send_welcome_sms if @user.mobile
    UserMailer.welcome_email(@user).deliver
    EmailLeague.update_pending_invitations @user
    logger.info "#{@user.username} creation -- OK"
    @user.user_settings = UserSettings.new
    #regBadge = Badge.find_by(type: "REGISTRATION")
    #UserBadge.create(user_id:@user.id, badge_id:regBadge.id, shown:false)
    render  json: {
                auth_token: @user.authentication_token,
                username: @user.username,
                fb_token: @user.uid
            }, 
            status: :created
  end

  def update 
    #warden.custom_failure;
    render :json => current_user.errors, status: :unprocessable_entity unless current_user.update_attributes!(user_update_params)
    #current_user.update_attributes!(user_update_params)
    logger.info "#{@user.username} update -- OK"  
    render  json: {
                auth_token: @user.authentication_token,
                username: @user.username,
                fb_token: @user.uid
            }, 
            status: :created
  end

  private
  def user_params
    params.require(:user).permit(:firstName, :lastName, :username, :mobile, :email, :password, :password_confirmation,:authentication_token)
  end
  private
  def user_update_params
    params.require(:user).permit(:firstName, :lastName, :mobile, :password, :password_confirmation, :current_password)
  end

end