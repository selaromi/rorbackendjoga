class UserSettingsController < ApplicationController

  # before_filter :owns_user_settings, only: [:edit, :update, :delete]
  def create
    user_settings = current_user.create_user_settings(user_settings_params)

    render :json => user_settings
  end

  def update
    user_settings = current_user.user_settings.update(update_user_settings_params)
    render :json => user_settings
  end

  def me
    render json: current_user.to_json(:only => [:id, :firstName, :lastName, :userName, :email, :uid],
                                      :include => {
                                                  :user_settings => {
                                          :except => [:user_id, :id]
                                      },
                                                   :predictions => {
                                                       :include => {:match => {:only => [:visitor_team, :local_team, :visitor_goals, :local_goals, :winner_goals, :played, :date, :stadium_id],
                                                                               :include => {
                                                                                   :local_team => {
                                                                                       :only => :name
                                                                                   },
                                                                                   :visitor_team => {
                                                                                       :only => :name
                                                                                   },
                                                                                   :stadium => {
                                                                                       :only => [:name, :city]
                                                                                   }
                                                                               }
                                                       }
                                                       }
                                                  },
                                                   :user_badges => {
                                                       :only => [:id, :shown, :won, :progress],
                                                       :include => {:badge => { :except => [:created_at, :updated_at]}}
                                                   },
                                                   :timeline_events => {
                                                       :only => [:created_at, :event_type,:data]
                                                   }
                                      }
    ), status: :ok
  end

  def friend_profile
    render json: User.find(friend_params[:user_id]).to_json(:only => [:id, :firstName, :lastName, :userName, :email, :uid],
                                      :include => {
                                          :user_settings => {
                                              :except => [:user_id, :id]
                                          },
                                          :user_badges => {
                                              :only => [:id, :shown, :won, :progress],
                                              :include => {:badge => { :except => [:created_at, :updated_at]}}
                                          },
                                          :timeline_events => {
                                              :only => [:created_at, :event_type,:data]
                                          }
                                      }
    ), status: :ok
  end

  private

  def friend_params
    params.require(:friend).permit(:user_id)
  end

  def user_settings_params
    params.require(:user_settings).permit(:country, :favorite_team)
  end

  def update_user_settings_params
    params.require(:user_settings).permit(:country, :favorite_team)
  end

end
