require 'pp'
class LeaguesController < ApplicationController
  ## put League create
  def create
    league = League.new(league_params)
    league.user_id = current_user.id
    return render json: league.errors, status: :bad_request unless league.save
    user_league = UserLeague.new(user_id:current_user.id, league_id:league.id, status: 1)
    return render json: user_league.errors, status: :bad_request  unless user_league.save
    current_user.update_badge '1LEAGUE', current_user.leagues.count + 1
    current_user.add_new_event TimelineEvent.create( event_type:"newLeague", data: league.to_json( :only => [:id,:name]))
    return render json: league, status: :created
  end

  ## put League update
  def update
    league = set_league
    return render_user_not_authorized "The league does not belong to the user" unless league.user_id == current_user.id
    return render json: league.errors, status: :bad_request unless league.update(league_params)
    render json:  league, status: :ok
  end


  def show
  	league = set_league
    page = if (show_league_params[:page].nil?) then 0 else show_league_params[:page].to_i * 10 end
    hash = Hash.new
    hash["league_info"] = league.as_json( only: [:id, :name, :description, :owner],
                                     :include => { 
                                                   :users => {
                                                               :ordinal => 1,
                                                               only: [:id, :username, :uid],
                                                               :include => {
                                                                              :user_settings => {
                                                                                                  only: [:points]
                                                                              }
                                                               }
                                                             },

                                                 }
                                   )
    inactive_users = UserLeague.where(league_id: hash['league_info']['id'], status: 0).collect {|user_league| user_league.user_id }
    hash["league_info"]["users"] = hash["league_info"]["users"].delete_if {|elem| inactive_users.include? elem["id"] }
    hash["league_info"]["users"] = hash["league_info"]["users"].sort!{|x,y| y["user_settings"]["points"] <=> x["user_settings"]["points"]}[page..page+9]

    # hash["league_info"]["users"]= hash["league_info"]["users"]
    
    owner = User.find(league.user_id).as_json( only: [:id, :username])
    hash["owner_info"] = owner
    render :json => hash.to_json , status: :ok
  end

  def update_league_positions
    leagues = League.find(:all)
    leagues.each do |l|
      usersLeague = UserSettings.where("user_id IN (?)", l.users)
      sortedUsers = usersLeague.sort_by(&:points).reverse
      sortedUsers.each_index do |user, index|
        userLeague = UserLeague.where("user_id = ? AND league_id = ?", user.user_id, l.id).first
        userLeague.update_attribute(:previous_position => userLeague.current_position, :current_position => index + 1)
      end
    end
  end

  def league_me
    league = set_league
    hash = Hash.new
    hash["league_info"] = league.as_json( only: [:id, :name, :description, :owner],
                                          :include => {
                                              :users => {
                                                  :ordinal => 1,
                                                  only: [:id, :username, :uid],
                                                  :include => {
                                                      :user_settings => {
                                                          only: [:points]
                                                      }
                                                  }
                                              },

                                          }
    )
    hash["league_info"]["users"] = hash["league_info"]["users"].sort!{|x,y| y["user_settings"]["points"] <=> x["user_settings"]["points"]}

    interestIndex = -1

    hash["league_info"]["users"].each_with_index do |user, index|
      if(user['id'] == current_user.id)
        interestIndex = index
        break
      end
    end

    hash["league_info"]["users"] = hash["league_info"]["users"][[0,interestIndex - 10].max..[interestIndex + 10, hash["league_info"]["users"].size].min]

    owner = User.find(league.user_id).as_json( only: [:id, :username])
    hash["owner_info"] = owner
    render :json => hash.to_json , status: :ok
  end

  def main_league
    league = League.find(0)
    page = if (show_league_params[:page].nil?) then 0 else show_league_params[:page].to_i * 10 end
    hash = Hash.new
    hash["league_info"] = league.as_json( only: [:id, :name, :description, :owner],
        :include => {
        :users => {
            :ordinal => 1,
            only: [:id, :username, :uid],
        :include => {
        :user_settings => {
            only: [:points]
    }
    }
    },

    }
    )
    hash["league_info"]["users"] = hash["league_info"]["users"].sort!{|x,y| y["user_settings"]["points"] <=> x["user_settings"]["points"]}[page..page+9]
    # hash["league_info"]["users"]= hash["league_info"]["users"]

    owner = User.find(league.user_id).as_json( only: [:id, :username])
    hash["owner_info"] = owner
    render :json => hash.to_json , status: :ok
  end

  def main_league_me
    league = League.find(0)
    hash = Hash.new
    hash["league_info"] = league.as_json( only: [:id, :name, :description, :owner],
                                          :include => {
                                                      :users => {
                                                                :ordinal => 1,
                                                                only: [:id, :username, :uid],
                                                                :include => {
                                                                            :user_settings => {
                                                                                              only: [:points]
                                                                                              }
                                                                }
                                                      },

                                          }
                          )
    hash["league_info"]["users"] = hash["league_info"]["users"].sort!{|x,y| y["user_settings"]["points"] <=> x["user_settings"]["points"]}

    interestIndex = -1

    hash["league_info"]["users"].each_with_index do |user, index|
      if(user['id'] == current_user.id)
        interestIndex = index
        break
      end
    end

    hash["league_info"]["users"] = hash["league_info"]["users"][[0,interestIndex - 10].max..[interestIndex + 10, hash["league_info"]["users"].size].min]

    owner = User.find(league.user_id).as_json( only: [:id, :username])
    hash["owner_info"] = owner
    render :json => hash.to_json , status: :ok
  end

  def pending_league_invitations
    pending_leagues = League.joins(:user_league).where("user_leagues.user_id = ? AND user_leagues.status = ?", current_user.id, 0)
    render :json => pending_leagues.to_json(:include => {:user => {:only =>[:id, :username]}}), status: :ok
  end

=begin
  def main_league
    page = if (show_league_params[:page].nil?) then 0 else show_league_params[:page].to_i * 10 end
    users = UserSettings.order(:points).reverse_order[page..page + 9]
    render json:  users.to_json(
        :only => [:points],:include => {:user => {:only => [:id, :username, :uid]}}
    ), :status=>200
  end

  def main_league_me
    users = UserSettings.order(:points).reverse_order
    interestIndex = -1;
    users.each_with_index do |user, index|
      if(user.user_id == current_user.id)
        interestIndex = index
        break
      end
    end

    users = users[[0,interestIndex - 10].max..[interestIndex + 10, users.size].min]

    render json:  users.to_json(
        :only => [:points],:include => {:user => {:only => [:id, :username, :uid]}}
    ), :status=>200
  end
=end

  def invite_users
    league = set_league
    return render_user_not_authorized "The league does not belong to the user" unless league.user_id == current_user.id
    unsuccessful_invites = Array.new()
    email_username_invites = invites_params[:other].split(',').map {|entry| entry.strip }
    facebook_invites = invites_params[:facebook].split(',').map {|entry| entry.strip }
    unsuccessful_invites.concat(create_invitation_by_username_email league,email_username_invites ) if email_username_invites.any? # The method returns the identifier of the unsuccessful invites
    unsuccessful_invites.concat(create_invitation_by_facebook league,facebook_invites ) if facebook_invites.any? # The method returns the identifier of the unsuccessful invites
    return render json: unsuccessful_invites.to_json, status: :ok
  end

  def create_invitation_by_facebook (league,facebook_invites)
    unsuccessful_invites_facebook = Array.new
    facebook_invites.each do |facebook_id|
      user = User.find_by(uid: facebook_id)
      if user
        unsuccessful_invites_facebook.concat( create_invitation_for_existing_user league, user )
      else
        unsuccessful_invites_facebook.concat( create_invitations_for_inexisting_user league, "facebook", facebook_id)
      end
    end
    return unsuccessful_invites_facebook
  end

  def create_invitation_by_username_email (league,email_username_invites)
    unsuccessful_invites_username_email = Array.new
    email_username_invites.each do |invitation|
      user = User.where('username=? OR email=?', invitation, invitation).first
      if user
        unsuccessful_invites_username_email.concat( create_invitation_for_existing_user league, user )
      else
        unsuccessful_invites_username_email.concat( create_invitations_for_inexisting_user league, "email", invitation )
      end
    end
    return unsuccessful_invites_username_email
  end

  def create_invitations_for_inexisting_user(league, invitation_method, invitation_data)

    if invitation_method == 'email'
      return [ invitation_data ] unless /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i.match(invitation_data) # if uses fake username, ignore it
      return [ invitation_data ] unless EmailLeague.create(email: invitation_data,
                                                       league_id:league.id,
                                                       invited_by: current_user.id)
      UserMailer.league_invitation_email(current_user,
                                         league,
                                         invitation_data).deliver
    else
      return invitation_data unless EmailLeague.create(facebook_id: invitation_data,
                                                       league_id:league.id,
                                                       invited_by: current_user.id)
       #UserMailer.league_invitation_email(current_user,
       #                                   league,
       #                                   invitation).deliver
    end
    return [] # No errors in the creation of the invitation
  end

  def create_invitation_for_existing_user(league, user)
    unless UserLeague.exists?({user_id:user.id, league_id:league.id})
      return invitation unless UserLeague.create(user_id:user.id,league_id:league.id,status:0)
      UserMailer.user_league_invitation_email(current_user,
                                                league,
                                                user).deliver
    else
      userLeague = UserLeague.where("user_id = ? AND league_id = ?", user.id, league.id).first
      userLeague.update_attribute(:status,0)
      UserMailer.user_league_resend_email(current_user,
                                          league,
                                          user).deliver
    end
    return [] # No errors in the creation of the invitation
  end

  def respond_invitation
    # render_user_not_authorized "that invitation ain't yours!" unless (respond_invitation_user_params[:user_id] == current_user.id)
    userLeague = UserLeague.where("user_id = ? AND league_id = ?", current_user.id, respond_invitation_league_params[:id]).first
    # puts userLeague.inspect
    # puts respond_invitation_user_params[:user_response]
    render_server_error "There was a problems updating your invitation" unless userLeague.update_attribute(:status,respond_invitation_user_params[:user_response])
    render json: {msg: "Everything went great"}, status: :created
  end

  def my_leagues
    render json: user_position_by_league(current_user.id).to_json, :status=>200
  end
  
  def leagues
    render json: user_position_by_league(leagues_params[:id]).to_json, :status=>200
  end

  private
  def invite_user_by_id league_id, user_id
    unless UserLeague.exists?({league_id: league_id, user_id: user_id})
      user_league = UserLeague.new({league_id: league_id, user_id: user_id})
      user_league.status = 0
      user_league.save
    end
  end

  def set_league
    league = League.find(show_league_params[:id])
  end

  def league_params
    params.require(:league).permit(:name, :description, :user_id,:id)
  end

  def main_league_params
    params.require(:league).permit(:page)
  end

  def show_league_params
    params.require(:league).permit(:id, :page)
  end

  def invitation_params
    params.require(:league).permit(:id, :invitations)
  end

  def invites_params
    params.require(:invitations).permit(:facebook,:other)
  end

  def leagues_params
    params.require(:friend).permit(:id)
  end

  def respond_invitation_user_params
    params.require(:user).permit(:user_response)
  end

  def respond_invitation_league_params
    params.require(:league).permit(:id)
  end

  def user_position_by_league id
    leagues = User.find(id).leagues.sort_by(&:id)
    leaguesInfo = Array.new
    leagues.each do |l|
      usersLeague = UserSettings.where("user_settings.user_id IN (?)", l.users.where('user_leagues.status = 1'))

      sortedUsers = usersLeague.sort_by(&:points).reverse
      sortedUsers.each_index do |index|
        if(sortedUsers[index].user_id == id)
          leaguesInfo << {
              league_id: l.id,
              league_name: l.name,
              user_pos: index + 1
          }
          break
        end
      end
    end
    return leaguesInfo
  end
end