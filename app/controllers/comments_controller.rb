class CommentsController < ApplicationController
  class Error < RuntimeError; end
  before_action :check_league_exists, only: [:viewAll, :create, :like ]
  before_action :check_user_belongs_to_league, except: [:destroy ]
  before_action :check_league_owns_comment, only: [:like, :dislike ]


  def viewAll
    render json: current_league.get_latest_comments, status: :ok
  end

  def create
    comment = Comment.new(comment_params)
    comment.sender_id = current_user
    comment.flagged = 0
    return render_bad_request comment.errors unless current_league.add_comment comment
    return render json: {error:'true'}, status: :created 
  end

  def destroy
    if(check_user_owns_comment)
      raise Error.new("There was an issue deleting your comment, don't worry! we'll fix it!") unless (current_comment.destroy)
      render json: {}, status: :ok
    else
      render_user_not_authorized %q{The comment doesn't belong to the user}
    end

    rescue Error => e
    # And render their message back to the user
    render json: { error: e.message }, status: :unprocessable_entity
  end

  def like
    raise Error.new("There was an issue with your vote, don't worry! we'll fix it!") unless 
                (current_comment.liked_by(current_user))
    render json: {}, status: :ok

    rescue Error => e
    # And render their message back to the user
    render json: { error: e.message }, status: :unprocessable_entity
  end

  def unlike
    raise Error.new("There was an issue with your vote, don't worry! we'll fix it!") unless 
                (current_comment.unliked_by(current_user))
    render json: {}, status: :ok

    rescue Error => e
    # And render their message back to the user
    render json: { error: e.message }, status: :unprocessable_entity
  end

  private

    def current_league
      League.find(comment_params[:league_id])
    end

    def current_comment
      Comment.find(current_comment_params[:id])
    end

    def user_belongs
      UserLeague.exists?(league_id: comment_params[:league_id], user_id: current_user.id, status: 1) 
    end

    def comment_params
      params.require(:comment).permit(:league_id,:receiver_id,:comment,:flagged, :original_message_id)
    end

    def current_comment_params
      params.require(:comment).permit(:id)
    end

    def check_league_exists
      raise ActiveRecord::RecordNotFound unless League.exists?(id: comment_params[:league_id])
    end

    def check_user_belongs_to_league
      render_user_not_authorized %q{The user doesn't belong to the league} unless user_belongs
    end 

    def check_user_owns_comment
      current_comment.sender_id == current_user
    end

    def check_league_owns_comment
      render_action_forbidden %q{The comment doesn't belong to the league} unless current_comment.league_id == comment_params[:league_id]
    end
end

