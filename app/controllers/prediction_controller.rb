class PredictionController < ApplicationController


  #POST /prediction/create
  def create
    #render :json =>  params
    #puts params[:matches]
    error = false
    params[:matches].each do |match|
      prediction = Prediction.new(match)
      prediction.user_id = current_user.id
      if(!prediction.save)
        error = true
        break
      end
    end
    if(!error)
      render_success
    else
      render json: { error: 'Error' }, status: :unprocessable_entity
    end
  end

  def update
    prediction = set_prediction
    if (prediction.user_id == current_user.id)
      prediction.visitor_goals = update_predictions_params[:visitor_goals]
      prediction.local_goals = update_predictions_params[:local_goals]
      prediction.winner_team = update_predictions_params[:winner_team]
      if (prediction.save())
        render json: {msg: "Everything went great"}, status: :created
      else
        render_server_error "There was a problems updating your prediction"
      end
    else
      render_user_not_authorized "that prediction ain't yours!"

    end


  end

  #POST /prediction
  def viewAllCurrentUser
    if(current_user.predictions.empty?)
      render_no_content %q{The user doesn't hasn't predicted any game yet}
    else
      render json: current_user.predictions.to_json( :only => [:id, :user_id, :match_id, :local_goals,:visitor_goals, :winner_team,:points_int]), status: 200
    end
  end

  def predictions
    #the line bellow can be used to get actual data for testing purposes
    #but the next line is the correct one
    #playedMatches = Match.where("date < ?", 1.day.ago)
    #why not use played = 1 as condition?. Maybe predictions could have a boolean value for its state,
    #updated when the points are calculated, in this way we wouldn't need to look for matches before
    #predictions
    playedMatches   = Match.where("date < ?", 300.days.from_now)
    predictions     = Prediction.where("match_id IN (?) AND user_id = ?", playedMatches, predictions_params[:id])
    
    render json:  predictions.to_json(
                                      :include => {:match => {  :only => [:visitor_team, :local_team, :visitor_goals, :local_goals, :winner_goals, :played, :date, :stadium_id],
                                                                                                        :include => {
                                                                                                                      :local_team => {
                                                                                                                                        :only => :name
                                                                                                                                      },
                                                                                                                      :visitor_team => {
                                                                                                                                        :only => :name
                                                                                                                                       },
                                                                                                                      :stadium => {
                                                                                                                                    :only => [:name, :city]
                                                                                                                                  }
                                                                                                                    }
                                                                                                     }
                                                                                          }
                                      ), :status=>200
  end

  private

  def set_prediction
    Prediction.find(update_predictions_params[:id]);
  end

  def predictions_params
    params.require(:friend).permit(:id)
  end

  def update_predictions_params
    params.require(:prediction).permit(:id,:local_goals,:visitor_goals,:winner_team);
  end

end
