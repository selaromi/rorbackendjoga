class TeamsController < ApplicationController
  skip_before_filter :authenticate_user_from_token!, :only => [ :all ]
  skip_before_filter :authenticate_user!, :only => [:all]

  # post /teams
  def all
    render json: Team.all.to_json, status: :ok
  end
end
