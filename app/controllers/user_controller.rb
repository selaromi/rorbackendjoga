class UserController < ApplicationController
  skip_before_filter :authenticate_user!,  :only => [:request_password_reset, :process_password_reset, :change_password]
  skip_before_filter :authenticate_user_from_token!, :only => [:request_password_reset, :process_password_reset, :change_password]

  def check_user_presence
    return render json:{exists:false}, status: :ok unless user_exists? check_user_params["username"],check_user_params["email"]
    render json:{exists:true}, status: :ok
  end

  def user_exists? username=nil , email=nil
    username.presence && User.find_by(username: username) ||
        email.presence && User.find_by(email: email)
  end

  def request_password_reset
    email = request_password_reset_params["email"]
    user  = User.find_by(email: email)
    if(user)
      user.reset_password_token = SecureRandom.urlsafe_base64(nil, false)
      user.save
      #url = url_for(:only_path => false, :controller => 'user', :action => 'process_password_reset')
      url = ENV['CLIENT_URL']
      url = url + '/' + user.username + '/' + user.reset_password_token
      UserMailer.password_reset_email(user, url).deliver
      return render json:{}, status: :ok
    else
      return render json:{}, status: :bad_request
    end
    return render json:{}, status: :bad_request
  end

  def error_warden
    render json: {}, status: :unprocessable_entity unless resource.save!
  end
=begin
  def process_password_reset
    username      = params["username"]
    resetToken   = params["reset_password_token"]
    user          = User.find_by(username: username)
    if(user)
      if(user.reset_password_token == resetToken)
        return render json:{}, status: :ok
      end
    else
      return render json:{}, status: :bad_request
    end
    return render json:{}, status: :bad_request
  end
=end

  def change_password
    username      = change_password_params["username"]
    resetToken    = change_password_params["reset_password_token"]
    newPassword   = change_password_params["password"]
    user          = User.find_by(username: username)
    if(user)
      if(user.reset_password_token == resetToken)
        user.password = newPassword
        user.reset_password_token = ''
        if(user.save)
          newToken  = reset_authentication_token(user)
          return render json: {
              auth_token: newToken,
              username: username
          }, status: :ok
        end
        return render json:{}, status: :bad_request
      end
    else
      return render json:{}, status: :bad_request
    end
    return render json:{}, status: :bad_request
  end

  private
  def check_user_params
    params.require(:invited_user).permit(:username,:email)
  end

  def request_password_reset_params
    params.require(:user).permit(:email)
  end

  def change_password_params
    params.require(:user).permit(:username, :reset_password_token, :password)
  end
end
