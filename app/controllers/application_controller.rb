class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :null_session
  # skip_before_action :verify_authenticity_token, if: :json_request?
  # This is our new function that comes before Devise's one, it asks for the email and 
  # the token to mitigate timming attacks

  # # This is Devise's authentication
  # before_filter :set_access_control_headers
  before_filter :authenticate_user_from_token!, :authenticate_user!, :cors_preflight_check
  after_filter :cors_set_access_control_headers
  rescue_from ActiveRecord::RecordNotFound, with: :render_bad_request
  rescue_from ActiveRecord::RecordInvalid, with: :render_bad_request
  # respond_to :json
  # For all responses in this controller, return the CORS access control headers.
  
  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = '*'
    headers['Access-Control-Max-Age'] = "1728000"
  end
  
  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.
  
  def cors_preflight_check
    if request.method == :options
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
      headers['Access-Control-Request-Method'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
      render :text => '', :content_type => 'text/plain'
    end
  end


  def render_404(exception = nil)
    if exception
        logger.info "Rendering 404: #{exception.message}"
    end
    render file: "#{Rails.root}/public/404.html", status: 404, layout: false
  end

  private
  def authenticate_user_from_token!
    return render_user_not_authorized "The signature is wrong (are you missing out any fields?)"  unless (params[:user].presence && params[:user][:username].presence && params[:user][:user_token].presence)
    username = params[:user][:username].presence
    user = username && (User.find_by_username(username) || User.find_by_email(username))
    # Notice how we use Devise.secure_compare to compare the token
    # in the database with the token given in the params, mitigating
    # timing attacks.
    if user && Devise.secure_compare(user.authentication_token, params[:user][:user_token])
      logger.info "#{user.username} authorized"
      sign_in user, store: false
    else
      logger.info "#{username} tried to log in with an outdated token"
      render_user_not_authorized "wrong username/token"
    end
  end

  def reset_authentication_token(resource = nil)
    new_token = loop do
      resource.authentication_token = SecureRandom.urlsafe_base64(nil, false)
      break resource.authentication_token unless User.exists?(authentication_token: resource.authentication_token)
    end
    logger.info "new token created for user #{resource.username}"
    return new_token
  end

  def login
    params.permit(:username, :user_token)
  end

  private
  def render_success
    render json: { msg: "ok" }, status: :ok
  end

  def render_created
    render
  end

  def render_updated
    render json: { msg:"update - OK"}, status: :ok
  end

  def render_no_content msg=""
    render json: { error: msg }, status: :no_content
  end

  def render_server_error msg=""
    render json: { error: msg}, status: :internal_server_error
  end

  def render_bad_request e
    render json: { error: e.message }, status: :bad_request
  end

  def render_record_not_found e
    render json: {error: e.message} , status: :not_found
  end

  def render_user_not_authorized msg=""
    render json: { error: msg }, status: :unauthorized
  end

  def render_action_forbidden msg=""
    render json: { error: msg }, status: :forbidden
  end
  # Error handling responses

  # def update_league_params
  #   params.permit(:name, :description, :user_id)
  # end
  # def devise_parameter_sanitizer
  #   if resource_class == User
  #     User::ParameterSanitizer.new(User, :user, params)
  #   else
  #     super
  #   end
  # end

  # def json_request?
  #   request.format.json?
  # end
end
