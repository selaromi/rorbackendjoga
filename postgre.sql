--to execute: psql -d joga_development -U postgres -f postgre.sql

delete from teams;

alter SEQUENCE teams_id_seq RESTART WITH 1;

delete from groups;

alter SEQUENCE groups_id_seq RESTART WITH 1;

delete from badges;

alter SEQUENCE badges_id_seq RESTART WITH 1;


insert into groups (name) values
('A'),
('B'),
('C'),
('D'),
('E'),
('F'),
('G'),
('H');


insert into teams (name, group_id, goals_scored, goals_received, games_won, games_lost, games_tied, points, region) values
('Brasil',1,0,0,0,0,0,0,'Am'),
('Croatia',1,0,0,0,0,0,0,'Eu'),
('Mexico',1,0,0,0,0,0,0,'Am'),
('Cameroon',1,0,0,0,0,0,0,'Af'),
('Spain',2,0,0,0,0,0,0,'Eu'),
('Netherlands',2,0,0,0,0,0,0,'Eu'),
('Chile',2,0,0,0,0,0,0,'Am'),
('Australia',2,0,0,0,0,0,0,'Oc'),
('Colombia',3,0,0,0,0,0,0,'Am'),
('Greece',3,0,0,0,0,0,0,'Eu'),
('Cote dIvoire',3,0,0,0,0,0,0,'Af'),
('Japan',3,0,0,0,0,0,0,'As'),
('Uruguay',4,0,0,0,0,0,0,'Am'),
('Costa Rica',4,0,0,0,0,0,0,'Am'),
('England',4,0,0,0,0,0,0,'Eu'),
('Italy',4,0,0,0,0,0,0,'Eu'),
('Switzerland',5,0,0,0,0,0,0,'Eu'),
('Ecuador',5,0,0,0,0,0,0,'Am'),
('France',5,0,0,0,0,0,0,'Eu'),
('Honduras',5,0,0,0,0,0,0,'Am'),
('Argentina',6,0,0,0,0,0,0,'Am'),
('Bosnia H.',6,0,0,0,0,0,0,'Eu'),
('Iran',6,0,0,0,0,0,0,'As'),
('Nigeria',6,0,0,0,0,0,0,'Af'),
('Germany',7,0,0,0,0,0,0,'Eu'),
('Portugal',7,0,0,0,0,0,0,'Eu'),
('Ghana',7,0,0,0,0,0,0,'Af'),
('USA',7,0,0,0,0,0,0,'Am'),
('Belgium',8,0,0,0,0,0,0,'Eu'),
('Algeria',8,0,0,0,0,0,0,'Af'),
('Russia',8,0,0,0,0,0,0,'Eu'),
('Korea Republic',8,0,0,0,0,0,0,'As');

delete from matches;


alter SEQUENCE matches_id_seq RESTART WITH 1;

delete from stadia;

alter Sequence stadia_id_seq RESTART WITH 1;
insert into stadia (name, city) values
('Arena Amazonia', 'Manaus'), -- 1
('Estadio Castelao', 'Fortaleza'), -- 2
('Estadio das Dunas', 'Natal'), -- 3
('Arena Pernambuco', 'Recife'), -- 4
('Arena Fonte Nova','Salvador'), -- 5
('Arena Pantanal','Cuiaba'), -- 6
('Estadio Nacional','Brasilia'), -- 7
('Estadio Mineirao','Belo Horizonte'), --8
('Arena de Sao Paulo','Sao Paulo'), -- 9
('Estadio do Maracana','Rio de Janeiro'), -- 10
('Arena da Baixada','Curitiba'), -- 11
('Estadio Beira-Rio','Porto Alegre'); -- 12


insert into matches (group_id, date, played, local_team, visitor_team, stadium_id) values
(1,timestamp '2014-06-12 20:00',0,1,2,9),
(1,timestamp '2014-06-13 16:00',0,3,4,3),
(1,timestamp '2014-06-17 19:00',0,1,3,2),
(1,timestamp '2014-06-18 21:00',0,4,2,1),
(1,timestamp '2014-06-23 20:00',0,4,1,7),
(1,timestamp '2014-06-23 20:00',0,2,3,4),

(2,timestamp '2014-06-13 19:00',0,5,6,5),
(2,timestamp '2014-06-13 21:00',0,7,8,6),
(2,timestamp '2014-06-18 19:00',0,5,7,10),
(2,timestamp '2014-06-18 16:00',0,8,6,12),
(2,timestamp '2014-06-23 16:00',0,8,5,11),
(2,timestamp '2014-06-23 16:00',0,6,7,9),

(3,timestamp '2014-06-14 16:00',0,9,10,8),
(3,timestamp '2014-06-14 01:00',0,11,12,4),
(3,timestamp '2014-06-19 16:00',0,9,11,7),
(3,timestamp '2014-06-19 22:00',0,12,10,3),
(3,timestamp '2014-06-24 19:00',0,12,9,6),
(3,timestamp '2014-06-24 20:00',0,10,11,2),

(4,timestamp '2014-06-14 19:00',0,13,14,2),
(4,timestamp '2014-06-14 21:00',0,15,16,1),
(4,timestamp '2014-06-19 19:00',0,13,15,9),
(4,timestamp '2014-06-20 16:00',0,16,14,4),
(4,timestamp '2014-06-24 16:00',0,16,13,3),
(4,timestamp '2014-06-24 16:00',0,14,15,8),

(5,timestamp '2014-06-15 16:00',0,17,18,7),
(5,timestamp '2014-06-15 19:00',0,19,20,12),
(5,timestamp '2014-06-20 19:00',0,17,19,5),
(5,timestamp '2014-06-20 22:00',0,20,18,11),
(5,timestamp '2014-06-25 19:00',0,20,17,1),
(5,timestamp '2014-06-25 20:00',0,18,19,10),

(6,timestamp '2014-06-15 22:00',0,21,22,10),
(6,timestamp '2014-06-16 19:00',0,23,24,11),
(6,timestamp '2014-06-21 16:00',0,21,23,8),
(6,timestamp '2014-06-21 21:00',0,24,22,6),
(6,timestamp '2014-06-25 16:00',0,24,21,12),
(6,timestamp '2014-06-25 16:00',0,22,23,5),

(7,timestamp '2014-06-16 16:00',0,25,26,5),
(7,timestamp '2014-06-16 22:00',0,27,28,3),
(7,timestamp '2014-06-21 19:00',0,25,27,2),
(7,timestamp '2014-06-22 21:00',0,28,26,1),
(7,timestamp '2014-06-26 16:00',0,28,25,4),
(7,timestamp '2014-06-26 16:00',0,26,27,7),

(8,timestamp '2014-06-17 16:00',0,29,30,8),
(8,timestamp '2014-06-17 21:00',0,31,32,6),
(8,timestamp '2014-06-22 16:00',0,29,31,10),
(8,timestamp '2014-06-22 19:00',0,32,30,12),
(8,timestamp '2014-06-26 20:00',0,32,29,9),
(8,timestamp '2014-06-26 20:00',0,30,31,11),

-- 9 - Round Of 16
(9,timestamp '2014-06-28 16:00',0,NULL,NULL,8),
(9,timestamp '2014-06-28 20:00',0,NULL,NULL,10),
(9,timestamp '2014-06-29 16:00',0,NULL,NULL,2),
(9,timestamp '2014-06-29 20:00',0,NULL,NULL,4),
(9,timestamp '2014-06-30 16:00',0,NULL,NULL,7),
(9,timestamp '2014-06-30 20:00',0,NULL,NULL,12),
(9,timestamp '2014-07-01 16:00',0,NULL,NULL,9),
(9,timestamp '2014-07-01 20:00',0,NULL,NULL,5),

-- 10 - Quarte Finals
(10,timestamp '2014-07-04 20:00',0,NULL,NULL,2),
(10,timestamp '2014-07-04 16:00',0,NULL,NULL,10),
(10,timestamp '2014-07-05 20:00',0,NULL,NULL,5),
(10,timestamp '2014-07-05 16:00',0,NULL,NULL,7),
-- 11 Semi Finals
(11,timestamp '2014-07-08 20:00',0,NULL,NULL,8),
(11,timestamp '2014-07-09 20:00',0,NULL,NULL,9),
-- 12 Third Place
(12,timestamp '2014-07-12 20:00',0,NULL,NULL,7),
-- 13 Final
(13,timestamp '2014-07-13 19:00',0,NULL,NULL,10);


insert into badges (badge_type, name, description, goal) values
('REGISTRATION', 'Golden ticket', 'Golden ticket',1),
('1PREDICTION', 'Rookie', 'Rookie',1),
('5PREDICTION', 'Rabbit Foot', 'Rabbit Foot',5),
('10PREDICTION', 'By the numbers', 'By the numbers',10),
('1SCORE', 'Lucky Shot', 'Lucky Shot',1),
('5SCORE', 'Chronicler', 'Chronicler',5),
('5AMERICA', 'New World', 'New World',5),
('5EUROPE', 'Old Continent', 'Old Continent',5),
('5AFRICA', 'Black Mamba', 'Black Mamba',5),
('5ASIA', 'Ninja Koala', 'Ninja Koala',5),
('1LEAGUE', 'Team Captain', 'Team Captain',1),
('FAV', 'National Pride', 'National Pride',1),
('CHAMP', 'Final Countdown', 'Final Countdown',1),
('QUARTER', 'Almost There', 'Almost There',1),
('SEMI', 'Fair and Square', 'Fair and Square',1),
('FINAL', 'Fair and Square', 'Fair and Square',1),
('PENALTIES', 'Dramatic Tension', 'Dramatic Tension',1);

insert into users (id, email, "firstName", "lastName",username) values
  (0,'hello@jogamas.com','Main User','Main User','jogamas');

insert into leagues (id,name,user_id,description) values
  (0,'Main League',0,'Global Standings');