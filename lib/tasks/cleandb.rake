require 'faker'
require 'better_errors'
require_relative 'player'
namespace :db do
  desc "Clean and fill database with sample data"

  task clean: :environment do



    userList = []
    userList.push (Player.new('Carlos','Crespo','macayaven@gmail.com','quwerty123','+34688248172', 'carlos','644870559'))
    userList.push (Player.new('Ignacio','Morales','ignacio.maldonado@gmail.com','quwerty123','+34665332477', 'nacho','347400005'))
    userList.push (Player.new('Luisfe','Arreaza','arreaza.l@gmail.com','quwerty123','+34625599487', 'luisfe','1809314'))

    Prediction.delete_all
    Comment.delete_all
    League.delete_all
    UserSettings.delete_all
    User.delete_all
    UserLeague.delete_all

    ActiveRecord::Base.connection.reset_pk_sequence! :predictions
    ActiveRecord::Base.connection.reset_pk_sequence! :comments
    ActiveRecord::Base.connection.reset_pk_sequence! :leagues
    ActiveRecord::Base.connection.reset_pk_sequence! :user_settings
    ActiveRecord::Base.connection.reset_pk_sequence! :users



    Faker::Config.locale = :en
    4.times do |n|
      firstName         = Faker::Name.first_name
      lastName          = Faker::Name.last_name
      email             = Faker::Internet.email
      password          = 'qwertyui'

      #begin
      #  username       =  Faker::Internet.user_name
      #end while                    (!User.find_by({username: username}).nil?)

      puts userList[n - 1].username
      user              = User.create!(   username:               userList[n - 1].username,
                                          lastName:               userList[n - 1].lname,
                                          firstName:              userList[n - 1].fname,
                                          email:                  userList[n - 1].email,
                                          mobile:                 userList[n - 1].mobile,
                                          uid:                    userList[n - 1].uid,
                                          password:               userList[n - 1].password,
                                          password_confirmation:  userList[n - 1].password)
      us                = UserSettings.new
      us.user_id        = user.id
      us.country        = Faker::Address.country
      us.favorite_team  = Faker::Address.country
      us.interests      = Faker::Lorem.sentence(word_count = 4)
      us.points         = 0
      us.city           = Faker::Address.city
      us.sex            = rand(2)
      us.age            = rand(10) + 21
      us.save


      34.times do |i|
        match = Match.find(i + 1)
        p = Prediction.new
        p.user_id = user.id
        p.match_id = match.id
        p.local_goals = rand(3)
        p.visitor_goals = rand(3)
        p.winner_team = (p.visitor_goals > p.local_goals)? match.visitor_team.id : match.local_team.id
        p.points = 0
        p.created_at = Time.now
        p.updated_at = Time.now
        p.prediction_changes = 1
        p.save

      end

    end

    l  = League.new
    l.name        = "We wanted to be astronauts"
    l.user_id     = 1
    l.description = "We wanted to be astronauts league"
    l.save

    users = User.all
    4.times do |n|
      user_league = UserLeague.new
      user_league.league_id = 1
      user_league.user_id   = n + 1
      user_league.status    = 1
      user_league.save
    end

  end
end