require 'faker'
require 'better_errors'
namespace :db do
  desc "Clean and fill database with sample data"
  
  task populate: :environment do
    
    Prediction.delete_all
    Comment.delete_all
    League.delete_all
    UserSettings.delete_all
    User.delete_all
    
    ActiveRecord::Base.connection.reset_pk_sequence! :predictions
    ActiveRecord::Base.connection.reset_pk_sequence! :comments
    ActiveRecord::Base.connection.reset_pk_sequence! :leagues
    ActiveRecord::Base.connection.reset_pk_sequence! :user_settings
    ActiveRecord::Base.connection.reset_pk_sequence! :users
    
    Faker::Config.locale = :en
    25.times do |n|
      firstName         = Faker::Name.first_name
      lastName          = Faker::Name.last_name
      email             = Faker::Internet.email
      password          = 'qwertyui'
      
      begin 
         username       =  Faker::Internet.user_name 
      end while (!User.find_by({username: username}).nil?)
      user              = User.create!(   username:               username,
                                          lastName:               lastName,
                                          firstName:              firstName,
                                          email:                  email,
                                          password:               password,
                                          password_confirmation:  password)
      us                = UserSettings.new
      us.user_id        = user.id
      us.country        = Faker::Address.country
      us.favorite_team  = Faker::Address.country
      us.interests      = Faker::Lorem.sentence(word_count = 4)
      us.points         = Faker::Number.number(2)
      us.city           = Faker::Address.city
      us.sex            = rand(2)
      us.age            = rand(88) + 11
      us.save
      
      34.times do |i|
          match = Match.find(i + 1)
          p = Prediction.new
          p.user_id = user.id
          p.match_id = match.id
          p.local_goals = rand(5)
          p.visitor_goals = rand(5)
          p.winner_team = (p.visitor_goals > p.local_goals)? match.visitor_team.id : match.local_team.id
          p.points = rand(300)
          p.created_at = Time.now
          p.updated_at = Time.now
          p.prediction_changes = 1
          p.save

      end

      5.times do |i|
        l             = League.new
        l.name        = Faker::Lorem.word
        l.user_id     = user.id
        l.description = Faker::Lorem.sentence(word_count = 4)
        l.save 
        5.times do |j|
          c           = Comment.new
          c.league_id = l.id
          c.sender_id = user
          c.comment   = Faker::Lorem.characters(char_count = rand(140))
          c.save
        end
      end


    end

    users = User.all
    leagues = League.all
    users_count = users.count
    leagues.each do |league|
      user_league = UserLeague.new
      user_league.league_id = league.id
      (1..5).each do
        begin
          user_league.user_id = users[rand(users_count)].id
          user_league.status = 1
        end while (!UserLeague.find_by({user_id: user_league.user_id, league_id:league.id}).nil?)
        user_league.save
      end
    end

  end
end