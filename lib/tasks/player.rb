class Player
  attr_accessor :fname, :mobile, :lname, :email, :password, :username, :uid

  def initialize(fname, lname, email, password, mobile, username,uid)
    # Instance variables
    @fname    = fname
    @lname    = lname
    @email    = email
    @password = password
    @mobile   = mobile
    @username = username
    @uid      = uid
  end
end