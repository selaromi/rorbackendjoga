module SmsCommunication
  def self.send_welcome_sms
    number_to_send_to = '+34665332477'
    self.send_sms number_to_send_to,'Bienvenido a joga+!'
  end

  def self.send_sms to, body
    self.twilio_client.account.sms.messages.create(
        :from => ENV['twilio_phone_number'],
        :to => to, #self.mobile Cuando tengamos
        :body => body
    )
  end

  def self.twilio_client
    @twilio_client ||= Twilio::REST::Client.new ENV['twilio_sid'] , ENV['twilio_token']
  end
end