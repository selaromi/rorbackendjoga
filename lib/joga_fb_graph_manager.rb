module JogaFbGraphManager
	def welcome_msg
		publish_msg self.oauth_token,
					'Just Joined the BEST quiniela site EVAAAAA',
					'http://morning-coast-2245.herokuapp.com/images/shirt-spain.png',
					'http://morning-coast-2245.herokuapp.com',
					'JogaMas',
					'FTW!!! The tiger forever'
	end

	def exchange_token current_token
		self.oauth_token = (fb_graph.exchange_token! current_token).access_token.to_s
		self.oauth_expires_at = Time.now + 60.days
	end

	private
	def fb_graph
		@fb_graph ||= FbGraph::Auth.new(ENV['facebook_app_id'], ENV['facebook_secret'])
	end

	def publish_msg oauth_token,message,picture,link,name,description
        FbGraph::User.me(oauth_token).feed!(
          :message => message,
          :picture => picture,
          :link => link,
          :name => name,
          :description => description
        )
	end
end