class User::ParameterSanitizer < Devise::ParameterSanitizer
    private
    def account_update
        default_params.permit(:firstName, :lastName, :username, :email, :password, :password_confirmation, :current_password)
    end
    def sign_up
        default_params.permit(:firstName, :lastName, :username, :email, :password, :password_confirmation, :current_password, :authentication_token)
    end
end