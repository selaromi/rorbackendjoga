class CreateForeignInvitations < ActiveRecord::Migration
  def change
    create_table :foreign_invitations do |t|
      t.string :email
      t.integer :league_id

      t.timestamps
    end
  end
end
