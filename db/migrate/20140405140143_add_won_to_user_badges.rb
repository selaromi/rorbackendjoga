class AddWonToUserBadges < ActiveRecord::Migration
  def change
    add_column :user_badges, :won, :boolean
  end
end
