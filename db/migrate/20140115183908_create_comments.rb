class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :league_id
      t.integer :sender_id
      t.integer :receiver_id
      t.integer :original_message_id
      t.string :comment
      t.integer :flagged

      t.timestamps
    end
  end
end
