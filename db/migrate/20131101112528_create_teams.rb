class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.integer :points
      t.integer :goals_scored
      t.integer :goals_received
      t.integer :group_id
      t.integer :games_won
      t.integer :games_lost
      t.integer :games_tied

      t.timestamps
    end

    add_index "teams", ["group_id"], name: "group_id_on_teams", using: :btree
  end
end
