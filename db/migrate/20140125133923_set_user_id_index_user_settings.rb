class SetUserIdIndexUserSettings < ActiveRecord::Migration
  def change
  	reversible do |dir|
      change_table :user_settings do |t|
        dir.up   { t.change :user_id, :integer, unique: true }
        dir.down { t.change :user_id, :integer }
      end
    end
  end
end
