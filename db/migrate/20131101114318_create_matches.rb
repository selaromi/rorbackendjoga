class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :group_id
      t.datetime :date
      t.integer :played
      t.integer :local_team
      t.integer :visitor_team
      t.integer :local_goals
      t.integer :visitor_goals
      t.integer :winner_team
      t.integer :stadium_id

      t.timestamps
    end

      add_index "matches", ["winner_team"], name: "winner_team_on_matches", using: :btree
      add_index "matches", ["local_team"], name: "local_team_on_matches", using: :btree
      add_index "matches", ["visitor_team"], name: "visitor_team_on_matches", using: :btree
      add_index "matches", ["stadium_id"], name: "stadium_id_on_matches", using: :btree
      add_index "matches", ["group_id"], name: "group_id_on_matches", using: :btree
  end
end
