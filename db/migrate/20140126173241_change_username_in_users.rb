class ChangeUsernameInUsers < ActiveRecord::Migration
  def change
  	reversible do |dir|
      change_table :users do |t|
        dir.up   { t.change :username, :string, unique: true }
        dir.down { t.change :username, :string }
      end
    end
  end
end
