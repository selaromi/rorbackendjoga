class AddPredictionsInformationToUserSettings < ActiveRecord::Migration
  def change
    change_table :user_settings do |t|
      t.integer :predictions_successful_america, default: 0
      t.integer :predictions_successful_asia, default: 0
      t.integer :predictions_successful_africa, default: 0
      t.integer :predictions_successful_oceania, default: 0
      t.integer :predictions_successful_europe, default: 0
      t.integer :predictions_perfect, default: 0
    end
  end
end
