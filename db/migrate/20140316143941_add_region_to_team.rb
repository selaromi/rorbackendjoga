class AddRegionToTeam < ActiveRecord::Migration
  def change
    change_table :teams do |t|
      t.string :region
    end
  end
end
