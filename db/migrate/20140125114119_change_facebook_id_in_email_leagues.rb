class ChangeFacebookIdInEmailLeagues < ActiveRecord::Migration
  def change
    reversible do |dir|
      change_table :email_leagues do |t|
        dir.up   { t.change :facebook_id, 'integer USING CAST(facebook_id AS integer)' }
        dir.down { t.change :facebook_id, 'string USING CAST(facebook_id AS string)' }
      end
    end

  end
end
