class CreateUserBadges < ActiveRecord::Migration
  def change
    create_table :user_badges do |t|
      t.integer :user_id
      t.integer :badge_id
      t.boolean :shown, default: false

      t.timestamps
    end

    add_index "user_badges", ["badge_id"], name: "badge_id_on_user_badges", using: :btree
    add_index "user_badges", ["user_id", "badge_id"], name: "user_id_and_user_badges", unique: true, using: :btree
    add_index "user_badges", ["user_id"], name: "user_id_on_user_badges", using: :btree
  end
end
