class ChangeNameToBadgesType < ActiveRecord::Migration
  def change
    change_table :badges do |t|
      t.remove(:type)
      t.string :badge_type
    end
  end
end
