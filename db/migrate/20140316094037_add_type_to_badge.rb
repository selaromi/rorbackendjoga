class AddTypeToBadge < ActiveRecord::Migration
  def change
    change_table :badges do |t|
      t.string :type
    end
  end
end
