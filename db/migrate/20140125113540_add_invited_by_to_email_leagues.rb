class AddInvitedByToEmailLeagues < ActiveRecord::Migration
  def change
    add_column :email_leagues, :invited_by, :integer
  end
end
