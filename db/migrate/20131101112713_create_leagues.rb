class CreateLeagues < ActiveRecord::Migration
  def change
    create_table :leagues do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
    add_index "leagues", ["user_id"], name: "user_id_on_leagues", using: :btree
  end
end
