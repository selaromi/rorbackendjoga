class CreateEmailLeagues < ActiveRecord::Migration
  def change
    create_table :email_leagues do |t|
      t.string :email
      t.string :league_id
      t.string :facebook_id

      t.timestamps
    end
  end
end
