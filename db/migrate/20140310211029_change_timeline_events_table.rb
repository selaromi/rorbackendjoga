class ChangeTimelineEventsTable < ActiveRecord::Migration
  def change
    change_table :timeline_events do |t|
      t.remove(:type)
      t.string :type
    end
  end
end
