class ChangeMobileToUserSettings < ActiveRecord::Migration
  def change
    change_table :user_settings do |t|
      t.remove(:mobile)
      t.string :mobile
    end
  end
end
