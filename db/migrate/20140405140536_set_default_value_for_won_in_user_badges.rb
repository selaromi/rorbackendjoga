class SetDefaultValueForWonInUserBadges < ActiveRecord::Migration
  def change
    change_table :user_badges do |t|
      t.remove(:won)
      t.boolean :won, default: false
    end
  end
end
