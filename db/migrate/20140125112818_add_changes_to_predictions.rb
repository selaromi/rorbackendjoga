class AddChangesToPredictions < ActiveRecord::Migration
  def change
    add_column :predictions, :changes, :integer
  end
end
