class AddColumnsToUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :city, :string
    add_column :user_settings, :sex, :integer
    add_column :user_settings, :interests, :text
    add_column :user_settings, :answer1, :integer
    add_column :user_settings, :answer2, :integer
    add_column :user_settings, :answer3, :integer
  end
end
