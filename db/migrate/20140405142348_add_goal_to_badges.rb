class AddGoalToBadges < ActiveRecord::Migration
  def change
    add_column :badges, :goal, :integer
  end
end
