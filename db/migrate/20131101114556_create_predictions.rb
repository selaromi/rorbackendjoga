class CreatePredictions < ActiveRecord::Migration
  def change
    create_table :predictions do |t|
      t.integer :user_id
      t.integer :match_id
      t.integer :local_goals
      t.integer :visitor_goals
      t.integer :winner_team
      t.string :points_int

      t.timestamps
    end

      add_index "predictions", ["winner_team"], name: "winner_team_on_predictions", using: :btree
      add_index "predictions", ["match_id"], name: "match_id_on_predictions", using: :btree
      add_index "predictions", ["user_id"], name: "user_id_on_predictions", using: :btree
  end
end
