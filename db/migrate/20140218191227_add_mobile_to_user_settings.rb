class AddMobileToUserSettings < ActiveRecord::Migration
  def change
    change_table :user_settings do |t|
      t.integer :mobile
    end
  end
end
