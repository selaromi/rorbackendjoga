class AlterChangesToPrediction < ActiveRecord::Migration
  def change
    reversible do |dir|
      change_table :predictions do |t|
        dir.up   { t.change :changes, :integer  }
        dir.down { t.change :prediction_changes, :integer }
      end
    end
  end
end
