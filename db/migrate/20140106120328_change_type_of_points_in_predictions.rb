class ChangeTypeOfPointsInPredictions < ActiveRecord::Migration
  def change
    remove_column :predictions, :points_int, :string
    add_column :predictions, :points, :integer
  end
end
