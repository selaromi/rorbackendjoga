class AddMobileToUser < ActiveRecord::Migration
  def change
  	change_table :users do |t|
      t.string :mobile
    end
  end
end
