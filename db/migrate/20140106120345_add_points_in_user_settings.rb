class AddPointsInUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :points, :integer
  end
end
