class AddProgressToUserBadge < ActiveRecord::Migration
  def change
    add_column :user_badges, :progress, :integer, default: 0
  end
end
