class CreateTimelineEvents < ActiveRecord::Migration
  def change
    create_table :timeline_events do |t|
      t.integer :type


      t.timestamps
    end
  end
end
