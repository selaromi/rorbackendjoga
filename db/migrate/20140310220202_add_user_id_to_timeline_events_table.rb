class AddUserIdToTimelineEventsTable < ActiveRecord::Migration
  def change
    change_table :timeline_events do |t|
      t.integer :user_id
    end
  end
end
