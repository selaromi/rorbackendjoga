class ChangeTypeToEventTypeTimelineEventsTable < ActiveRecord::Migration
  change_table :timeline_events do |t|
    t.remove(:type)
    t.string :event_type
  end
end
