class AddPositionToLeagues < ActiveRecord::Migration
  def change
    change_table :user_leagues do |t|
      t.integer :current_position
      t.integer :previous_position
    end
  end
end
