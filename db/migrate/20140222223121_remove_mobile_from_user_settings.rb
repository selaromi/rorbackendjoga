class RemoveMobileFromUserSettings < ActiveRecord::Migration
  def change
	change_table :user_settings do |t|
      t.remove(:mobile)
    end
  end
end
