class AddAgeToUserSettings < ActiveRecord::Migration
  def change
    add_column :user_settings, :age, :integer, limit:3
  end
end
