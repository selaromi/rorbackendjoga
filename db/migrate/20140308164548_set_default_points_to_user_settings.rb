class SetDefaultPointsToUserSettings < ActiveRecord::Migration
  def self.up
    change_column :user_settings, :points, :int, :default => 0
  end

  def self.down
    # You can't currently remove default values in Rails
    raise ActiveRecord::IrreversibleMigration, "Can't remove the default"
  end
end
