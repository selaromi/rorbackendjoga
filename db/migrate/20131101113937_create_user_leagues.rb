class CreateUserLeagues < ActiveRecord::Migration
  def change
    create_table :user_leagues do |t|
      t.integer :user_id
      t.integer :league_id
      t.integer :status

      t.timestamps
    end

      add_index "user_leagues", ["league_id"], name: "league_id_on_user_leagues", using: :btree
  	  add_index "user_leagues", ["user_id", "league_id"], name: "user_id_2_on_user_leagues", unique: true, using: :btree
  	  add_index "user_leagues", ["user_id"], name: "user_id_on_user_leagues", using: :btree
  end
end
