class ChangeDataFromTextToJsonOnTimelineEventsTable < ActiveRecord::Migration
  change_table :timeline_events do |t|

    t.json :data
  end
end
