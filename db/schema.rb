# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140405142658) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "badges", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "badge_type"
    t.integer  "goal"
  end

  create_table "comments", force: true do |t|
    t.integer  "league_id"
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.integer  "original_message_id"
    t.string   "comment"
    t.integer  "flagged"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "email_leagues", force: true do |t|
    t.string   "email"
    t.string   "league_id"
    t.integer  "facebook_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "invited_by"
  end

  create_table "foreign_invitations", force: true do |t|
    t.string   "email"
    t.integer  "league_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "leagues", force: true do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  add_index "leagues", ["user_id"], name: "user_id_on_leagues", using: :btree

  create_table "matches", force: true do |t|
    t.integer  "group_id"
    t.datetime "date"
    t.integer  "played"
    t.integer  "local_team"
    t.integer  "visitor_team"
    t.integer  "local_goals"
    t.integer  "visitor_goals"
    t.integer  "winner_team"
    t.integer  "stadium_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "matches", ["group_id"], name: "group_id_on_matches", using: :btree
  add_index "matches", ["local_team"], name: "local_team_on_matches", using: :btree
  add_index "matches", ["stadium_id"], name: "stadium_id_on_matches", using: :btree
  add_index "matches", ["visitor_team"], name: "visitor_team_on_matches", using: :btree
  add_index "matches", ["winner_team"], name: "winner_team_on_matches", using: :btree

  create_table "predictions", force: true do |t|
    t.integer  "user_id"
    t.integer  "match_id"
    t.integer  "local_goals"
    t.integer  "visitor_goals"
    t.integer  "winner_team"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "points"
    t.integer  "prediction_changes"
  end

  add_index "predictions", ["match_id"], name: "match_id_on_predictions", using: :btree
  add_index "predictions", ["user_id"], name: "user_id_on_predictions", using: :btree
  add_index "predictions", ["winner_team"], name: "winner_team_on_predictions", using: :btree

  create_table "stadia", force: true do |t|
    t.string   "name"
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "teams", force: true do |t|
    t.string   "name"
    t.integer  "points"
    t.integer  "goals_scored"
    t.integer  "goals_received"
    t.integer  "group_id"
    t.integer  "games_won"
    t.integer  "games_lost"
    t.integer  "games_tied"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "region"
  end

  add_index "teams", ["group_id"], name: "group_id_on_teams", using: :btree

  create_table "timeline_events", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "event_type"
    t.json     "data"
  end

  create_table "user_badges", force: true do |t|
    t.integer  "user_id"
    t.integer  "badge_id"
    t.boolean  "shown",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "won",        default: false
    t.integer  "progress",   default: 0
  end

  add_index "user_badges", ["badge_id"], name: "badge_id_on_user_badges", using: :btree
  add_index "user_badges", ["user_id", "badge_id"], name: "user_id_and_user_badges", unique: true, using: :btree
  add_index "user_badges", ["user_id"], name: "user_id_on_user_badges", using: :btree

  create_table "user_leagues", force: true do |t|
    t.integer  "user_id"
    t.integer  "league_id"
    t.integer  "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "current_position"
    t.integer  "previous_position"
  end

  add_index "user_leagues", ["league_id"], name: "league_id_on_user_leagues", using: :btree
  add_index "user_leagues", ["user_id", "league_id"], name: "user_id_2_on_user_leagues", unique: true, using: :btree
  add_index "user_leagues", ["user_id"], name: "user_id_on_user_leagues", using: :btree

  create_table "user_settings", force: true do |t|
    t.integer "user_id"
    t.string  "country"
    t.string  "favorite_team"
    t.integer "points",                         default: 0
    t.string  "city"
    t.integer "sex"
    t.text    "interests"
    t.integer "answer1"
    t.integer "answer2"
    t.integer "answer3"
    t.integer "age"
    t.integer "predictions_successful_america", default: 0
    t.integer "predictions_successful_asia",    default: 0
    t.integer "predictions_successful_africa",  default: 0
    t.integer "predictions_successful_oceania", default: 0
    t.integer "predictions_successful_europe",  default: 0
    t.integer "predictions_perfect",            default: 0
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "firstName"
    t.string   "lastName"
    t.string   "username"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.string   "mobile"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

end
