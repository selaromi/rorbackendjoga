Joga::Application.routes.draw do

  post "*all" => "application#cors_preflight_check", :constraints => { :method => "OPTIONS" }
  # Devise routes
  devise_for :users, 
  :controllers => { 
    :registrations => "registrations",
    :sessions => "sessions",
    :omniauth_callbacks => "users/omniauth_callbacks"
    }, 
  :path_names => { 
    :sign_in => 'login', 
    :sign_out => 'logout',
    :password => 'secret', 
    :confirmation => 'verification', 
    registration: 'register' }, 
  :token_authentication_key => 'authentication_key' 

  devise_scope :user do
      resources :user_settings
      get 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_session_facebook
      # get 'auth/:provider/callback', to: 'sessions#createFromFacebook'
      # get 'auth/failure', to: redirect('/')
      # get  'signoutFacebook', to: 'sessions#destroyFromFacebook', as: 'signout'

  end

  post 'matches' => 'matches#all'
  post 'stadia' => 'stadia#all'
  post 'teams' => 'teams#all'

  #resources :stadia
  
  post 'leagues/create' => 'leagues#create'
  post 'leagues/update' => 'leagues#update'
  post 'league' => 'leagues#show'
  post 'leagues/mainleague' => 'leagues#main_league'
  post 'leagues/globalstanding' => 'leagues#main_league_me'
  post 'leagues/leaguestanding' => 'leagues#league_me'
  post 'leagues/respondinvitation' => 'leagues#respond_invitation'
  post 'leagues/pendinginvitations' => 'leagues#pending_league_invitations'

  post 'leagues/invite' => 'leagues#invite_users'
  post 'leagues/comments' => 'comments#viewAll'
  post 'leagues/comments/add' => 'comments#create'
  post 'leagues/comments/remove' => 'comments#destroy'
  post 'leagues/comments/like' => 'comments#like'
  post 'leagues/comments/unlike' => 'comments#unlike'

  #resources :teams
  
  post 'predictions/create' => 'prediction#create'
  post 'predictions' => 'prediction#viewAllCurrentUser'
  post 'predictions/updatePoints' => 'prediction#updatePoints'
  post 'predictions/friendpredictions' => 'prediction#predictions'
  post 'predictions/update' => 'prediction#update'
  post 'user/me' => 'user_settings#me'
  post 'user/friend' => 'user_settings#friend_profile'
  
  post 'league/friendleagues' => 'leagues#leagues'
  post 'league/myleagues' => 'leagues#my_leagues'


  post 'users/check' => 'user#check_user_presence'
  post 'users/forgotpass'  => 'user#request_password_reset'
  post 'users/resetpass'  => 'user#change_password'

  #post 'users/processresetpass/' => 'user#process_password_reset'
  #get  'users/processresetpass/:username/:reset_password_token'  => 'user#process_password_reset'

  # namespace :api, defaults: {format: :json} do
  #   devise_scope :user do
  #     resource :registrations, only: [:create]
  #   end
  #   # resources :task_lists, only: [:index, :create, :update, :destroy, :show] do
  #   #   resources :tasks, only: [:index, :create, :update, :destroy]
  #   # end
  # end

  get '*a' => 'application#render_404'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
