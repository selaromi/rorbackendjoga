Joga::Application.configure do
  # config.lograge.enabled = true
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # I18n.enforce_available_locales = false
  
  ActionMailer::Base.smtp_settings = {
    port: 1025,
    address: "localhost"
  }
  ActionMailer::Base.delivery_method = :smtp

  # ENV['FACEBOOK_APP_ID']      = "212391085617615"
  # ENV['FACEBOOK_SECRET']      = "2188014d67196523d0f351ce5cd27bac"
  ENV['CLIENT_URL']           = "http://morning-coast-2245.herokuapp.com"

  config.lograge.enabled = false
  
  # custom_options can be a lambda or hash
  # if it's a lambda then it must return a hash
  config.lograge.custom_options = lambda do |event|
    unwanted_keys = %w[format action controller]
    params = event.payload[:params].reject { |key,_| unwanted_keys.include? key }
 
    # capture some specific timing values you are interested in
    {:params => params }
  end
end
