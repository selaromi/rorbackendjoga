GROUP A
Match	Date - Time	Venue			Results		
1		12/06 17:00	Sao Paulo			A1	-	A2	
2		13/06 13:00	Natal				A3	-	A4	
17		17/06 16:00	Fortaleza			A1	-	A3	
18		18/06 15:00	Manaus				A4	-	A2	
33		23/06 17:00	Brasilia			A4	-	A1	
34		23/06 17:00	Recife				A2	-	A3	
GROUP B
Match	Date - Time	Venue			Results		
3		13/06 16:00	Salvador			B1	-	B2	
4		13/06 18:00	Cuiaba				B3	-	B4	
19		18/06 19:00	Rio De Janeiro		B1	-	B3	
20		18/06 13:00	Porto Alegre		B4	-	B2	
35		23/06 13:00	Curitiba			B4	-	B1	
36		23/06 13:00	Sao Paulo			B2	-	B3	
GROUP C
Match	Date - Time	Venue			Results		
5		14/06 13:00	Belo Horizonte		C1	-	C2	
6		14/06 19:00	Recife				C3	-	C4	
21		19/06 13:00	Brasilia			C1	-	C3	
22		19/06 19:00	Natal				C4	-	C2	
37		24/06 16:00	Cuiaba				C4	-	C1	
38		24/06 17:00	Fortaleza			C2	-	C3	
GROUP D
Match	Date - Time	Venue			Results		
7		14/06 16:00	Fortaleza			D1	-	D2	
8		14/06 21:00	Manaus				D3	-	D4	
23		19/06 16:00	Sao Paulo			D1	-	D3	
24		20/06 13:00	Recife				D4	-	D2	
39		24/06 13:00	Natal				D4	-	D1	
40		24/06 13:00	Belo Horizonte		D2	-	D3	
GROUP E
Match	Date - Time	Venue			Results		
9		15/06 13:00	Brasilia			E1	-	E2	
10		15/06 16:00	Porto Alegre		E3	-	E4	
25		20/06 16:00	Salvador			E1	-	E3	
26		20/06 19:00	Curitiba			E4	-	E2	
41		25/06 16:00	Manaus				E4	-	E1	
42		25/06 17:00	Rio De Janeiro		E2	-	E3	
GROUP F
Match	Date - Time	Venue			Results		
11		15/06 19:00	Rio De Janeiro		F1	-	F2	
12		16/06 16:00	Curitiba			F3	-	F4	
27		21/06 13:00	Belo Horizonte		F1	-	F3	
28		21/06 18:00	Cuiaba				F4	-	F2	
43		25/06 13:00	Porto Alegre		F4	-	F1	
44		25/06 13:00	Salvador			F2	-	F3	
GROUP G
Match	Date - Time	Venue			Results		
13		16/06 13:00	Salvador			G1	-	G2	
14		16/06 19:00	Natal				G3	-	G4	
29		21/06 16:00	Fortaleza			G1	-	G3	
30		22/06 15:00	Manaus				G4	-	G2	
45		26/06 13:00	Recife				G4	-	G1	
46		26/06 13:00	Brasilia			G2	-	G3	
GROUP H
Match	Date - Time	Venue			Results		
15		17/06 13:00	Belo Horizonte		H1	-	H2	
16		17/06 18:00	Cuiaba				H3	-	H4	
31		22/06 19:00	Rio De Janeiro		H1	-	H3	
32		22/06 13:00	Porto Alegre		H4	-	H2	
47		26/06 17:00	Sao Paulo			H4	-	H1	
48		26/06 17:00	Curitiba			H2	-	H3		