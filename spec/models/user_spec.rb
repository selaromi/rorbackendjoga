require 'spec_helper'
require 'pp'

describe User do

    it "has a valid Factory" do 
        expect(build(:user)).to be_valid
    end

   	it "is valid when email format is valid" do
		addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
		addresses.each do |valid_address|
			expect(build(:user, email: valid_address)).to be_valid
		end
	end

	it "is invalid without a firstName" do
		user = build(:user, firstName: nil)
		expect(user).to have(1).errors_on(:firstName)
	end

	it "is invalid without a lastName" do
		user = build(:user, lastName: nil)
		expect(user).to have(1).errors_on(:lastName)
	end
	it "is invalid without a username" do
		user = build(:user, username: nil)
		expect(user).to have(1).errors_on(:username)
	end

	it "is invalid when email is not present" do
		user = build(:user, email: nil)
		expect(user).to have(2).errors_on(:email)
	end

	it "is invalid when password is not present" do
		user = build(:user, password: nil)
		expect(user).to have(1).errors_on(:password)
	end

	it "is invalid when password is too short" do
		user = build(:user, password: "aaaaa")
		expect(user).to have(1).errors_on(:password)
	end

	it "is invalid when password doesn't match password_confirmation" do
		user = build(:user, password_confirmation: "mismatch")
		expect(user).to have(1).errors_on(:password_confirmation)
	end

	it "is invalid when email is already taken" do
		create(:user, email:"Alo@se.com")
		user = build(:user, email: "Alo@se.com")
		expect(user).to have(1).errors_on(:email)
	end

	it "is invalid when email format is invalid" do
		addresses = %w[user@foo,com user_at_foo.org example.user@foo.
		foo@bar_baz.com foo@bar+baz.com foo@bar..com]
		addresses.each do |invalid_address|
			user = build(:user, email: invalid_address)
			expect(user).not_to be_valid

		end
	end
end
