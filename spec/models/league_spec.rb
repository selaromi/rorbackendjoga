require 'spec_helper'

describe League do
    it "has a valid Factory" do 
        expect(build(:league)).to be_valid
    end
	it "is invalid without a name" do
		league = build(:league, name: nil)
		expect(league).to have(1).errors_on(:name)
	end
	it "is invalid without a description" do
		league = build(:league, description: nil)
		expect(league).to have(1).errors_on(:description)
	end
	it "is invalid without a user_id" do
		league = build(:league, user_id: nil)
		expect(league).to have(1).errors_on(:user_id)
	end

end
