require 'spec_helper'

describe UserSettings do
	it "has a valid Factory" do 
    	expect(build(:user_league)).to be_valid
   	end

	it 'is invalid without a status' do
		user_league = build_stubbed(:user_league,status:nil)
		expect(user_league).to have(1).errors_on(:status)
	end

	it 'is invalid without a league_id' do
		user_league = build_stubbed(:user_league,league_id:nil)
		expect(user_league).to have(1).errors_on(:league_id)
	end

	it 'is invalid without a user_id' do
		user_league = build_stubbed(:user_league,user_id:nil)
		expect(user_league).to have(1).errors_on(:user_id)
	end

end