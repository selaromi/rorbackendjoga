require 'spec_helper'
require 'pp'

describe UserSettings do
	it "has a valid Factory" do 
    	expect(build_stubbed(:user_settings)).to be_valid
   	end
	it 'is invalid without a favorite team' do
		user_settings = build_stubbed(:user_settings,favorite_team:nil)
		expect(user_settings).to have(1).errors_on(:favorite_team)
	end

end
