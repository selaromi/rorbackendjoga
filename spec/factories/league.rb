require 'faker'

FactoryGirl.define do
  factory :league do
  	association :user
  	name { Faker::Name.name }
  	description { Faker::Name.name }
  end
end