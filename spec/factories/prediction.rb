require 'faker'

FactoryGirl.define do
  factory :prediction do
  	association :user
  	association :game
  	score_team_a { Faker::Number.digit }
  	score_team_b { Faker::Number.digit }
  end
end