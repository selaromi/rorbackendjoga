require 'faker'

FactoryGirl.define do
  factory :game do
  	team_a { Faker::Address.country }
  	team_b { Faker::Address.country }
  	city { Faker::Address.city }
  	stadium { Faker::Name.name }
  	game_date "01/01/14 23:59:59"
  	points_given { Faker::Number.digit}
  end
end