require 'faker'

FactoryGirl.define do
  factory :user_settings do
    association :user
  	country { Faker::Address.country }
  	favorite_team { Faker::Address.country }
  end
end