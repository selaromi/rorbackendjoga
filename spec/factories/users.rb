require 'faker'

FactoryGirl.define do
  factory :user do
  	firstName { Faker::Name.first_name }
  	lastName { Faker::Name.last_name }
  	username { Faker::Internet.user_name }
    email { Faker::Internet.email } 
    password { Faker::Internet.password }
    password_confirmation { password }
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
  end
end