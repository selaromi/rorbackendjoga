require 'faker'

FactoryGirl.define do
  factory :user_league do
    association :user
    association :league
  	status { 0 }
  end
end